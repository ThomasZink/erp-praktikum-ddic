# Tables

References need to be placed before the field definition.  
In the following example the field PRICE of type /DMO/SUPPLEMENT_PRICE is defined in table /DMO/BOOKSUPPL_M.  
It references the field CURRENCY_CODE of the same table as unit for the PRICE.  

> define table /DMO/BOOKSUPPL_M {  
> `[...]`  
>  CURRENCY_CODE         : /DMO/CURRENCY_CODE;  
> `[...]`  
>  **@Semantics.amount.currencyCode : '/DMO/BOOKSUPPL_M.CURRENCY_CODE'**  
>  PRICE                 : /DMO/SUPPLEMENT_PRICE;  
> `[...]`  
> }  

### 1. Create table ZRAPH_##_ROOMRSV

Give the table the description: 'RAP HandsOn: Hotel Room Reservations'  
Define the following fields:

| Field name            | Kind of type         | Type                    | Is key | Nullable | Reference                                                        |
| --------------------- | -------------------- | ----------------------- | ------ | -------- | ---------------------------------------------------------------- |
| CLIENT                | Predefined DDIC type | CLNT                    | yes    | no       |                                                                  |
| ROOM_RSV_UUID         | Data Element         | SYSUUID_X16             | yes    | no       |                                                                  |
| PARENT_UUID           | Data Element         | SYSUUID_X16             | no     | yes      |                                                                  |
| ROOM_RSV_ID           | Data Element         | ZRAPH_##_ROOM_RSV_ID    | no     | no       |                                                                  |
| HOTEL_ID              | Data Element         | ZRAPH_##_HOTEL_ID       | no     | yes      |                                                                  |
| BEGIN_DATE            | Data Element         | /DMO/BEGIN_DATE         | no     | yes      |                                                                  |
| END_DATE              | Data Element         | /DMO/END_DATE           | no     | yes      |                                                                  |
| ROOM_TYPE             | Data Element         | ZRAPH_##_ROOM_TYPE      | no     | yes      |                                                                  |
| ROOM_RSV_PRICE        | Data Element         | ZRAPH_##_ROOM_RSV_PRICE | no     | yes      | @Semantics.amount.currencyCode: 'ZRAPH_##_ROOMRSV.CURRENCY_CODE' |
| CURRENCY_CODE         | Data Element         | /DMO/CURRENCY_CODE      | no     | yes      |                                                                  |
| LOCAL_LAST_CHANGED_AT | Data Element         | TIMESTAMPL              | no     | yes      |                                                                  |
  
[Solution](./solutions/ZRAPH_%23%23_ROOMRSV.txt)
  
### 2. Create table ZRAPH_##_HOTEL

Above the table definition give the following metadata:  
Give the table the description: 'RAP HandsOn: Hotel Master Data'  
Define the following fields:

| Field name     | Kind of type         | Type                    | Is key | Nullable | Reference                                                         |
| -------------- | -------------------- | ----------------------- | ------ | -------- | ----------------------------------------------------------------- |
| CLIENT         | Predefined DDIC type | CLNT                    | yes    | no       |                                                                   |
| HOTEL_ID       | Data Element         | ZRAPH_##_HOTEL_ID       | yes    | no       |                                                                   |
| NAME           | Data Element         | ZRAPH_##_HOTEL_NAME     | no     | yes      |                                                                   |
| CITY           | Data Element         | /DMO/CITY               | no     | yes      |                                                                   |
| COUNTRY        | Data Element         | LAND1                   | no     | yes      |                                                                   |
  
[Solution](./solutions/ZRAPH_%23%23_HOTEL.txt)
