@AbapCatalog.viewEnhancementCategory: [#NONE]
@AccessControl.authorizationCheck: #NOT_REQUIRED
@EndUserText.label: 'Reservation'
@Metadata.ignorePropagatedAnnotations: true
@ObjectModel.usageType:{
    serviceQuality: #X,
    sizeCategory: #S,
    dataClass: #MIXED
}
define view entity ZRAPH_##_RESERVATION
  as select from zraph_##_roomrsv as roomrsv
  association[1..1] to zraph_##_hotel as _hotel on _hotel.hotel_id = roomrsv.hotel_id
{
  key room_rsv_uuid         as RoomReservationUUID,
      parent_uuid           as ParentUUID,
      room_rsv_id           as RoomReservationID,
      hotel_id              as HotelID,
      begin_date            as BeginDate,
      end_date              as EndDate,
      room_type             as RoomType,
      @Semantics.amount.currencyCode : 'CurrencyCode'
      room_rsv_price        as RoomReservationPrice,
      currency_code         as CurrencyCode,
      local_last_changed_at as LocalLastChangedAt,
      
      _hotel.name           as Name,
      _hotel.city           as City,
      _hotel.country        as Country
}
